memory,500,m
geomtyp=xyz
nosym
noorient
geometry={
H          0.00000        0.00000        0.00000

H          0.00000        0.00000        0.74188

}
basis={ default,svp }
{uks,b3lyp,direct,dsyevd=0,dsyevd=0,dsyevd=0;wf,2,1,0}
basis={ default,sto-3g }
{uhf,direct,dsyevd=0,dsyevd=0,dsyevd=0;wf,2,1,0}
{uks,LDA,direct,dsyevd=0,dsyevd=0,dsyevd=0;maxit,0;wf,2,1,0}
{ibba,bonds=1}
