memory,500,m
geomtyp=xyz
nosym
noorient
geometry={
N          0.00000        0.00000        0.00000

H          0.00000        0.00000        1.03673

}
basis={ default,tzvp }
{hf,direct;wf,8,1,2}
{df-ks,PBE,direct;maxit,0;wf,8,1,2}
{ibba,bonds=1}
