memory,500,m
geomtyp=xyz
nosym
noorient
geometry={
N          0.00000        0.00000        0.00000

H          0.00000        0.00000        1.02405

H          0.99716        0.00000       -0.23314

}
basis={ default,avdz }
{df-ks,B3LYP3,direct;wf,9,1,1}
{ibba,bonds=1}
