memory,500,m
geomtyp=xyz
nosym
noorient
geometry={
F          0.00000        0.00000        0.00000

}
basis={ default,qzvp }
{hf,direct;wf,9,1,1}
{df-ks,M06-2X,direct;maxit,0;wf,9,1,1}
{ibba,bonds=1}
