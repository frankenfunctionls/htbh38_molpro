memory,500,m
geomtyp=xyz
nosym
noorient
geometry={
H          0.00000        0.00000        0.00000

}
basis={ default,avtz }
{hf,direct;wf,1,1,1}
{df-ks,M06-2X,direct;maxit,0;wf,1,1,1}
{ibba,bonds=1}
