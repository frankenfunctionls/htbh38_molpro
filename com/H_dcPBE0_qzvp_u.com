memory,500,m
geomtyp=xyz
nosym
noorient
geometry={
H          0.00000        0.00000        0.00000

}
basis={ default,qzvp }
{uhf,direct;wf,1,1,1}
{uks,PBE0,direct;maxit,0;wf,1,1,1}
{ibba,bonds=1}
