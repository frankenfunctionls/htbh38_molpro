memory,500,m
geomtyp=xyz
nosym
noorient
geometry={
H          0.00000        0.00000       -0.86029

O          0.00000        0.00000        0.32902

H          0.00000        0.00000       -1.77191

}
basis={ default,qzvp }
{uks,MM06-L,direct;wf,10,1,2}
{ibba,bonds=1}
