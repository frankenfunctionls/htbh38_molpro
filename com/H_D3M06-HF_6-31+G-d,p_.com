memory,500,m
geomtyp=xyz
nosym
noorient
geometry={
H          0.00000        0.00000        0.00000

}
basis={default,sto-3g}
{hf,direct;wf,1,1,1}

basis={ default,6-31+G(d,p) }
{ks,M06-HF,direct;disp;wf,1,1,1}
{ibba,bonds=1}
