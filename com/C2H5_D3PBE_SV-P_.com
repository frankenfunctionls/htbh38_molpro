memory,500,m
geomtyp=xyz
nosym
noorient
geometry={
C          0.00000        0.00000        0.00000

C          0.00000        0.00000        1.49014

H          1.01377        0.00000        1.89114

H         -0.84855        0.37413       -0.55287

H         -0.50106       -0.88768        1.89585

H         -0.52501        0.86748        1.89105

H          0.77218       -0.51270       -0.55357

}
basis={ default,SV(P) }
{ks,PBE,direct;disp;wf,17,1,1}
{ibba,bonds=1}
