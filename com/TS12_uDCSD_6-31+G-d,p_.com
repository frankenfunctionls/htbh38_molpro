memory,500,m
geomtyp=xyz
nosym
noorient
geometry={
H          0.00000        0.00000       -0.86029

O          0.00000        0.00000        0.32902

H          0.00000        0.00000       -1.77191

}
basis={default,sto-3g}
{hf,direct;wf,10,1,2}

basis={ default,6-31+G(d,p) }
{hf;wf,10,1,2}
{uDCSD,direct;wf,10,1,2}
{ibba,bonds=1}
