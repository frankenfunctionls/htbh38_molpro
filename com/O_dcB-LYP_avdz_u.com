memory,500,m
geomtyp=xyz
nosym
noorient
geometry={
O          0.00000        0.00000        0.00000

}
basis={ default,avdz }
{uhf,direct;wf,8,1,2}
{uks,B-LYP,direct;maxit,0;wf,8,1,2}
{ibba,bonds=1}
