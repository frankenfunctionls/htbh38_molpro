memory,500,m
geomtyp=xyz
nosym
noorient
geometry={
H          0.00000        0.00000       -0.86029

O          0.00000        0.00000        0.32902

H          0.00000        0.00000       -1.77191

}
basis={ default,svp }
{uhf,direct;wf,10,1,2}
{uks,B3LYP3,direct;maxit,0;wf,10,1,2}
{ibba,bonds=1}
