memory,500,m
geomtyp=xyz
nosym
noorient
geometry={
H          0.00000        0.00000        0.00000

H          0.00000        0.00000        0.74188

}
basis={default,sto-3g}
{uhf,direct;wf,2,1,0}

basis={ default,6-31+G(d,p) }
{uks,PBE0MOL,direct;wf,2,1,0}
{ibba,bonds=1}
