memory,500,m
geomtyp=xyz
nosym
noorient
geometry={
F          0.00000        0.00000        0.00000

H          0.00000        0.00000        0.91538

}
basis={ default,avtz }
{hf;wf,10,1,0}
{uCCSD(T)-f12,df_basis=avtz/mp2fit,df_basis_exch=avtz/jkfit,direct;wf,10,1,0}
{ibba,bonds=1}
