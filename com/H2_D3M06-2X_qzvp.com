memory,500,m
geomtyp=xyz
nosym
noorient
geometry={
H          0.00000        0.00000        0.00000

H          0.00000        0.00000        0.74188

}
basis={ default,qzvp }
{df-ks,M06-2X,direct;disp;wf,2,1,0}
{ibba,bonds=1}
