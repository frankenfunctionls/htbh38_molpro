memory,500,m
geomtyp=xyz
nosym
noorient
geometry={
F          0.00000        0.00000        0.00000

}
basis={default,sto-3g}
{hf,direct;wf,9,1,1}

basis={ default,6-31+G(d,p) }
{ks,SLATER,direct;wf,9,1,1}
{ibba,bonds=1}
