memory,500,m
geomtyp=xyz
nosym
noorient
geometry={
S          0.00000        0.00000        0.10252

H          0.00000        0.96625       -0.82015

H          0.00000       -0.96625       -0.82015

}
basis={default,sto-3g}
{uhf,direct;wf,18,1,0}

basis={ default,6-31+G(d,p) }
{uhf,direct;wf,18,1,0}
{uks,M06-HF,direct;maxit,0;wf,18,1,0}
{ibba,bonds=1}
