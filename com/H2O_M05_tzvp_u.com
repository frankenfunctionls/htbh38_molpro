memory,500,m
geomtyp=xyz
nosym
noorient
geometry={
O          0.00000        0.00000        0.00000

H          0.00000        0.00000        0.95691

H          0.92636        0.00000       -0.23987

}
basis={ default,tzvp }
{uks,M05,direct;wf,10,1,0}
{ibba,bonds=1}
