memory,500,m
geomtyp=xyz
nosym
noorient
geometry={
S          0.00000        0.00000        0.00000

H          0.00000        0.00000        1.34020

}
basis={default,sto-3g}
{uhf,direct;wf,17,1,1}

basis={ default,6-31+G(d,p) }
{uks,PBE0MOL,direct;wf,17,1,1}
{ibba,bonds=1}
