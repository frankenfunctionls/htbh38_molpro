memory,500,m
geomtyp=xyz
nosym
noorient
geometry={
H          0.00000        0.00000        0.00000

}
basis={ default,svp }
{hf;wf,1,1,1}
{df-lrmp2,direct;wf,1,1,1}
{ibba,bonds=1}
