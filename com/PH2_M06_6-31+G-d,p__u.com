memory,500,m
geomtyp=xyz
nosym
noorient
geometry={
P          0.00000        0.00000       -0.11566

H          1.02013        0.00000        0.86743

H         -1.02013        0.00000        0.86743

}
basis={default,sto-3g}
{uhf,direct;wf,17,1,1}

basis={ default,6-31+G(d,p) }
{uks,M06,direct;wf,17,1,1}
{ibba,bonds=1}
