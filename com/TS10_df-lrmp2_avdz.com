memory,500,m
geomtyp=xyz
nosym
noorient
geometry={
C          0.00029       -1.14229        0.00000

H         -1.05596       -1.38474        0.00000

H          0.52017       -1.40739        0.91245

H          0.52017       -1.40739       -0.91245

H          0.01156        0.16010        0.00000

O          0.00029        1.36164        0.00000

}
basis={ default,avdz }
{hf;wf,18,1,2}
{df-lrmp2,direct;wf,18,1,2}
{ibba,bonds=1}
