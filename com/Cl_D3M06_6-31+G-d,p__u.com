memory,500,m
geomtyp=xyz
nosym
noorient
geometry={
Cl         0.00000        0.00000        0.00000

}
basis={default,sto-3g}
{uhf,direct;wf,17,1,1}

basis={ default,6-31+G(d,p) }
{uks,M06,direct;disp;wf,17,1,1}
{ibba,bonds=1}
