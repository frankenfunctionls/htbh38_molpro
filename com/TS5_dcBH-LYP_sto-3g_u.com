memory,500,m
geomtyp=xyz
nosym
noorient
geometry={
H          0.00000        0.00000        0.00000

H          0.00000        0.00000        0.92947

H          0.00000        0.00000       -0.92947

}
basis={ default,sto-3g }
{uhf,direct;wf,3,1,1}
{uks,BH-LYP,direct;maxit,0;wf,3,1,1}
{ibba,bonds=1}
