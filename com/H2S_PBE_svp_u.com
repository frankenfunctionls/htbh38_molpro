memory,500,m
geomtyp=xyz
nosym
noorient
geometry={
S          0.00000        0.00000        0.10252

H          0.00000        0.96625       -0.82015

H          0.00000       -0.96625       -0.82015

}
basis={ default,svp }
{uks,PBE,direct;wf,18,1,0}
{ibba,bonds=1}
