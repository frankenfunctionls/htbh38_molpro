memory,500,m
geomtyp=xyz
nosym
noorient
geometry={
Cl         0.00000        0.00000        0.00000

}
basis={ default,avtz }
{uhf,direct;wf,17,1,1}
{uks,BH-LYP,direct;maxit,0;wf,17,1,1}
{ibba,bonds=1}
