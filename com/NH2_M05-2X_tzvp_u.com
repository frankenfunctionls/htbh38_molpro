memory,500,m
geomtyp=xyz
nosym
noorient
geometry={
N          0.00000        0.00000        0.00000

H          0.00000        0.00000        1.02405

H          0.99716        0.00000       -0.23314

}
basis={ default,tzvp }
{uks,M05-2X,direct;wf,9,1,1}
{ibba,bonds=1}
