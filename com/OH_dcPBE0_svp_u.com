memory,500,m
geomtyp=xyz
nosym
noorient
geometry={
O          0.00000        0.00000        0.00000

H          0.00000        0.00000        0.96890

}
basis={ default,svp }
{uhf,direct;wf,9,1,1}
{uks,PBE0,direct;maxit,0;wf,9,1,1}
{ibba,bonds=1}
