memory,500,m
geomtyp=xyz
nosym
noorient
geometry={
O         -0.30106       -0.10805       -0.00001

H         -0.42795        0.85157        0.00002

H          1.01549       -0.10037        0.00012

H          1.82097        0.11319       -0.00007

}
basis={ default,tzvp }
{df-ks,PW91,direct;wf,11,1,1}
{ibba,bonds=1}
