memory,500,m
geomtyp=xyz
nosym
noorient
geometry={
C          0.00000        0.00000        0.00000

H          0.00000        0.00000        1.07732

H          0.93298        0.00000       -0.53866

H         -0.93298        0.00000       -0.53866

}
basis={ default,qzvp }
{hf,direct;wf,9,1,1}
{df-ks,M06-2X,direct;maxit,0;wf,9,1,1}
{ibba,bonds=1}
