memory,500,m
geomtyp=xyz
nosym
noorient
geometry={
C          0.00000        0.00000        0.00000

H          0.00000        0.00000        1.07732

H          0.93298        0.00000       -0.53866

H         -0.93298        0.00000       -0.53866

}
basis={ default,avtz }
{uks,BH-LYP,direct;disp;wf,9,1,1}
{ibba,bonds=1}
