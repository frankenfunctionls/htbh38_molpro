memory,500,m
geomtyp=xyz
nosym
noorient
geometry={
Cl         0.01882       -0.81730        0.00000

H         -0.47049        0.56948        0.00000

O          0.01882        1.66558        0.00000

}
basis={ default,avdz }
{uhf,direct;wf,26,1,2}
{uks,M06-HF,direct;maxit,0;wf,26,1,2}
{ibba,bonds=1}
