memory,500,m
geomtyp=xyz
nosym
noorient
geometry={
Cl         0.01882       -0.81730        0.00000

H         -0.47049        0.56948        0.00000

O          0.01882        1.66558        0.00000

}
basis={default,sto-3g}
{hf,direct;wf,26,1,2}

basis={ default,6-31+G(d,p) }
{ks,M06-2X,direct;wf,26,1,2}
{ibba,bonds=1}
