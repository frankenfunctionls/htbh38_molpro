memory,500,m
geomtyp=xyz
nosym
noorient
geometry={
H          0.00000        0.00000        0.00000

H          0.00000        0.00000        0.92947

H          0.00000        0.00000       -0.92947

}
{basis,svp}
{ks,b3lyp}
basis={ default,svp }
{hf;wf,3,1,1}
{df-lrmp2,direct;wf,3,1,1}
{ibba,bonds=1}
