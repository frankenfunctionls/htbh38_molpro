memory,500,m
geomtyp=xyz
nosym
noorient
geometry={
H          0.00000        0.00000        0.00000

H          0.00000        0.00000        0.74188

}
basis={ default,avtz }
{uhf,direct;wf,2,1,0}
{uks,M06-L,direct;maxit,0;wf,2,1,0}
{ibba,bonds=1}
