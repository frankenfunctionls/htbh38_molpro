memory,500,m
geomtyp=xyz
nosym
noorient
geometry={
Cl         0.00000        0.00000        0.00000

}
basis={ default,tzvp }
{hf,direct;wf,17,1,1}
{df-ks,PBE,direct;maxit,0;wf,17,1,1}
{ibba,bonds=1}
