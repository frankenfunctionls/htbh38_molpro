memory,500,m
geomtyp=xyz
nosym
noorient
geometry={
H          0.00048       -1.34063        0.00000

Cl         0.00000        0.20325        0.00000

H         -0.00048       -2.11466        0.00000

}
basis={ default,svp }
{df-ks,PBE0MOL,direct;wf,19,1,1}
{ibba,bonds=1}
