memory,500,m
geomtyp=xyz
nosym
noorient
geometry={
O          0.00000        0.00000        0.00000

H          0.00000        0.00000        0.95691

H          0.92636        0.00000       -0.23987

}
basis={ default,avtz }
{uks,VWN80,direct;wf,10,1,0}
{ibba,bonds=1}
