memory,500,m
geomtyp=xyz
nosym
noorient
geometry={
H          0.00000        0.00000        0.00000

H          0.00000        0.00000        0.92947

H          0.00000        0.00000       -0.92947

}
basis={ default,sto-3g }
{ks,M06-2X,direct;disp;wf,3,1,1}
{ibba,bonds=1}
