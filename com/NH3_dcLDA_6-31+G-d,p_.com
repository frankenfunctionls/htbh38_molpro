memory,500,m
geomtyp=xyz
nosym
noorient
geometry={
N          0.00000        0.00000        0.11289

H          0.00000        0.93802       -0.26341

H          0.81235       -0.46901       -0.26341

H         -0.81235       -0.46901       -0.26341

}
basis={default,sto-3g}
{hf,direct;wf,10,1,0}

basis={ default,6-31+G(d,p) }
{hf,direct;wf,10,1,0}
{ks,LDA,direct;maxit,0;wf,10,1,0}
{ibba,bonds=1}
