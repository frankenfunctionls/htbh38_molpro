memory,500,m
geomtyp=xyz
nosym
noorient
geometry={
H          0.14657       -1.12839        0.00000

F          0.00000        0.33042        0.00000

H         -0.14657       -1.84541        0.00000

}
basis={ default,avdz }
{uks,B3LYP3,direct;disp;wf,11,1,1}
{ibba,bonds=1}
