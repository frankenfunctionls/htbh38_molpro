memory,500,m
geomtyp=xyz
nosym
noorient
geometry={
O          0.00000        0.00000        0.00000

}
basis={default,sto-3g}
{uhf,direct;wf,8,1,2}

basis={ default,6-31+G(d,p) }
{uks,LDA,direct;wf,8,1,2}
{ibba,bonds=1}
