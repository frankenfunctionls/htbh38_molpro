memory,500,m
geomtyp=xyz
nosym
noorient
geometry={
N          0.00000        0.00000        0.11289

H          0.00000        0.93802       -0.26341

H          0.81235       -0.46901       -0.26341

H         -0.81235       -0.46901       -0.26341

}
basis={ default,avtz }
{hf;wf,10,1,0}
{uCCSD(T)-f12,df_basis=avtz/mp2fit,df_basis_exch=avtz/jkfit,direct;wf,10,1,0}
{ibba,bonds=1}
