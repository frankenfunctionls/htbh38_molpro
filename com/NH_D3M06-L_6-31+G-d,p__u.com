memory,500,m
geomtyp=xyz
nosym
noorient
geometry={
N          0.00000        0.00000        0.00000

H          0.00000        0.00000        1.03673

}
basis={default,sto-3g}
{uhf,direct;wf,8,1,2}

basis={ default,6-31+G(d,p) }
{uks,M06-L,direct;disp;wf,8,1,2}
{ibba,bonds=1}
