memory,500,m
geomtyp=xyz
nosym
noorient
geometry={
F          0.00000        0.00000        0.00000

H          0.00000        0.00000        0.91538

}
basis={ default,sto-3g }
{ks,B3LYP3,direct;disp;wf,10,1,0}
{ibba,bonds=1}
