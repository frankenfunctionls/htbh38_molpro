memory,500,m
geomtyp=xyz
nosym
noorient
geometry={
O          0.00000        0.00000        0.00000

H          0.00000        0.00000        0.95691

H          0.92636        0.00000       -0.23987

}
basis={default,sto-3g}
{hf,direct;wf,10,1,0}

basis={ default,6-31+G(d,p) }
{ks,M06-HF,direct;disp;wf,10,1,0}
{ibba,bonds=1}
