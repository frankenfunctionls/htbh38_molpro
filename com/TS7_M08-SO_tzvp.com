memory,500,m
geomtyp=xyz
nosym
noorient
geometry={
C          0.24412        0.59992        1.70242

H         -0.67560        0.27848        2.17294

H          0.35191        1.66379        1.53767

H          1.14069        0.06579        1.98782

H          0.05716        0.13997        0.39711

Cl        -0.13758       -0.33809       -0.95942

}
basis={ default,tzvp }
{df-ks,M08-SO,direct;wf,27,1,1}
{ibba,bonds=1}
