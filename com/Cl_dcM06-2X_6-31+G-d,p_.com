memory,500,m
geomtyp=xyz
nosym
noorient
geometry={
Cl         0.00000        0.00000        0.00000

}
basis={default,sto-3g}
{hf,direct;wf,17,1,1}

basis={ default,6-31+G(d,p) }
{hf,direct;wf,17,1,1}
{ks,M06-2X,direct;maxit,0;wf,17,1,1}
{ibba,bonds=1}
