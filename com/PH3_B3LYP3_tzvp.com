memory,500,m
geomtyp=xyz
nosym
noorient
geometry={
P          0.00000        0.00000        0.12641

H          1.19134        0.00000       -0.63206

H         -0.59567       -1.03173       -0.63206

H         -0.59567        1.03173       -0.63206

}
basis={ default,tzvp }
{df-ks,B3LYP3,direct;wf,18,1,0}
{ibba,bonds=1}
