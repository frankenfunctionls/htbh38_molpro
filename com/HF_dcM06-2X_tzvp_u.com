memory,500,m
geomtyp=xyz
nosym
noorient
geometry={
F          0.00000        0.00000        0.00000

H          0.00000        0.00000        0.91538

}
basis={ default,tzvp }
{uhf,direct;wf,10,1,0}
{uks,M06-2X,direct;maxit,0;wf,10,1,0}
{ibba,bonds=1}
