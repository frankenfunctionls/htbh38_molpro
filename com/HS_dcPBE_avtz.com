memory,500,m
geomtyp=xyz
nosym
noorient
geometry={
S          0.00000        0.00000        0.00000

H          0.00000        0.00000        1.34020

}
basis={ default,avtz }
{hf,direct;wf,17,1,1}
{df-ks,PBE,direct;maxit,0;wf,17,1,1}
{ibba,bonds=1}
