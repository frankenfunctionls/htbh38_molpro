memory,500,m
geomtyp=xyz
nosym
noorient
geometry={
H          0.00000        0.00000        0.00000

}
basis={ default,sto-3g }
{hf;wf,1,1,1}
{rDCSD,direct;wf,1,1,1}
{ibba,bonds=1}
