memory,500,m
geomtyp=xyz
nosym
noorient
geometry={
H          1.26210       -0.22010        0.00000

S          0.00000        0.22315        0.00000

H         -0.50058       -1.11545        0.00000

H         -0.76152       -2.23491        0.00000

}
basis={ default,svp }
{uks,M06,direct;wf,19,1,1}
{ibba,bonds=1}
