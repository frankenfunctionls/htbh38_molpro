memory,500,m
geomtyp=xyz
nosym
noorient
geometry={
H          0.00000        0.00000        0.00000

}
basis={ default,SV(P) }
{hf,direct;wf,1,1,1}
{ks,LDA,direct;maxit,0;wf,1,1,1}
{ibba,bonds=1}
