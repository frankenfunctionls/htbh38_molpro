memory,500,m
geomtyp=xyz
nosym
noorient
geometry={
C          0.00000        0.26481        0.00000

H          1.05343        0.51667        0.00000

H         -0.52663        0.51702        0.91225

H         -0.52663        0.51702       -0.91225

H         -0.00026       -1.11777        0.00000

H          0.00008       -2.02183        0.00000

}
basis={ default,svp }
{df-ks,PBE0,direct;disp;wf,11,1,1}
{ibba,bonds=1}
