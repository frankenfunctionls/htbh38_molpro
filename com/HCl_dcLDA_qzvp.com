memory,500,m
geomtyp=xyz
nosym
noorient
geometry={
Cl         0.00000        0.00000        0.00000

H          0.00000        0.00000        1.27445

}
basis={ default,qzvp }
{hf,direct;wf,18,1,0}
{df-ks,LDA,direct;maxit,0;wf,18,1,0}
{ibba,bonds=1}
