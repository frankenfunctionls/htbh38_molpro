memory,500,m
geomtyp=xyz
nosym
noorient
geometry={
S          0.00000        0.00000        0.00000

H          0.00000        0.00000        1.34020

}
basis={ default,tzvp }
{hf,direct;wf,17,1,1}
{df-ks,M06-L,direct;maxit,0;wf,17,1,1}
{ibba,bonds=1}
