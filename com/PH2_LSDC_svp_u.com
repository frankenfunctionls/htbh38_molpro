memory,500,m
geomtyp=xyz
nosym
noorient
geometry={
P          0.00000        0.00000       -0.11566

H          1.02013        0.00000        0.86743

H         -1.02013        0.00000        0.86743

}
basis={ default,svp }
{uks,LSDC,direct;wf,17,1,1}
{ibba,bonds=1}
