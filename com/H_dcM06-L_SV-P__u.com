memory,500,m
geomtyp=xyz
nosym
noorient
geometry={
H          0.00000        0.00000        0.00000

}
basis={ default,SV(P) }
{uhf,direct;wf,1,1,1}
{uks,M06-L,direct;maxit,0;wf,1,1,1}
{ibba,bonds=1}
