memory,500,m
geomtyp=xyz
nosym
noorient
geometry={
N          0.00000        0.00000        0.00000

H          0.00000        0.00000        1.03673

}
basis={ default,sto-3g }
{uks,M06-2X,direct;disp;wf,8,1,2}
{ibba,bonds=1}
