memory,500,m
geomtyp=xyz
nosym
noorient
geometry={
P          0.00000        0.00000       -0.11566

H          1.02013        0.00000        0.86743

H         -1.02013        0.00000        0.86743

}
basis={ default,avtz }
{uhf,direct;wf,17,1,1}
{uks,M06-L,direct;maxit,0;wf,17,1,1}
{ibba,bonds=1}
